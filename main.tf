module "quake" {
  source  = "AdrienneCohea/nquakesv/nomad"
  version = "0.0.6"
  #source = "/home/acohea/Code/github.com/AdrienneCohea/terraform-nquakesv-nomad"

  service_meta  = {
    "dns_a_record"     = "quake"
    "dns_managed_zone" = "adriennecohea-io"
  }
}

data "consul_services" "all" {}

data "consul_service" "service" {
  for_each = toset(data.consul_services.all.names)
  name = each.key
}

data "google_dns_managed_zone" "personal" {
  name = "adriennecohea-io"
}

resource "google_dns_record_set" "quake" {
  name = "quake.${data.google_dns_managed_zone.personal.dns_name}"
  type = "A"
  ttl  = 300

  managed_zone = data.google_dns_managed_zone.personal.name

  rrdatas = ["34.105.41.152"]
}
